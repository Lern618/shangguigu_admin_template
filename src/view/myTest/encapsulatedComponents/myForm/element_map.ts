export default {
    //  输入框
    input: {
        component: 'el-input',
        props: {
            placeholder: '请输入内容',
            clearable: true,
            style: { width: '200px' }
        }
    },
    //  复选
    checkbox: {
        component: 'MyCheckBox',
        props: {
            clearable: true,
        }
    },
    // 单选
    radio: {
        component: 'MyRadio',
        props: {
            clearable: true,
        }
    },
    //  下拉框
    select: {
        component: 'el-select',
        props: {
            placeholder: '请选择',
            clearable: true,
            style: { width: '200px' }
        }
    },
    // 数字输入框
    inputNumber: {
        component: 'el-input-number',
        props: {
            placeholder: '请输入内容',
            clearable: true,
        }
    },
    //  多行文本
    textarea: {
        component: 'el-input',
        props: {
            type: 'textarea',
            placeholder: '请输入内容',
            style: { width: '400px' }
        }
    },
    //  日期
    date: {
        component: 'el-date-picker',
        props: {
            type: 'date',
            placeholder: '请选择日期',
        }
    },
}