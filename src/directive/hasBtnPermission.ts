import pinia from '@/store'
import useUserStore from '@/store/modules/user'
const useStore = useUserStore()
export const hasBtnPermission = (app: any) => {
  app.directive('btn', {
    //  使用该自定义指令的Dom，在组件挂载完成之后执行一次
    mounted(el, options) {
      console.log(el, options, useStore.btnPermissionList)
      // 如果找不到 btn.Trademark.add 就移除按钮
      if (!useStore.btnPermissionList.includes(options.value)) {
        el.parentNode.removeChild(el)
      }
    },
  })
}
