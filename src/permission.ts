/**
 *  路由的全局守卫 路由鉴权
 */

//  引入router
import router from '@/router/index'
//  引入路由样式
import { RouteRecordRaw } from 'vue-router'
// 引入进度条
import Nprogress from 'nprogress'
// 引入进度条样式
import '@/style/nprogress.css'
// 引入小仓库
import pinia from '@/store/index'
import useUserStore from '@/store/modules/user'
import setting from './setting'
const userStore = useUserStore(pinia)

//  关闭进度条自带加载圈
Nprogress.configure({ showSpinner: false })
//  路由的前置守卫
router.beforeEach(async (to: any, from: any, next: any) => {
  // 标题
  document.title = `${setting.title} - ${to.meta.title}`
  // 进度条开始
  Nprogress.start()
  // 获取用户的token
  const token = userStore.token
  const username = userStore.username
  // 已经登录过
  if (token) {
    //  登录过 去访问登录界面
    if (to.path === '/login') {
      next({ path: '/' })
    } else {
      // 登录成功后,去访问其他页面
      //    有用户信息
      if (username) {
        next()
      } else {
        //  么有用户信息,调用接口获取用户信息
        try {
          await userStore.userInfo()
          /* 如果刷新的时候是异步路由,有可能获取到用户信息、异步路由还没有加载完毕,出现空白的效果
           *  next() 直接放行
           *  next({...to}) 页面加载完成之后再放行
           * */
          next({ ...to })
        } catch (error) {
          //token过期:获取不到用户信息了
          //用户手动修改本地存储token
          //退出登录->用户相关的数据清空
          await userStore.userLoginout()
          next({ path: '/login', query: { redirect: to.path } })
        }
      }
    }
  } else {
    // 未登录
    if (to.path === '/login') {
      next()
    } else {
      // 未登录 访问其他路由
      next({ path: '/login', query: { redirect: to.path } })
    }
  }

  //   console.log(to, from, next)
  //  放行函数
})
//  路由的后置守卫
router.afterEach((to, from) => {
  Nprogress.done()
})

/**
 * //第一个问题:任意路由切换实现进度条业务 ---nprogress
   //第二个问题:路由鉴权(路由组件访问权限的设置)
   //全部路由组件:登录|404|任意路由|首页|数据大屏|权限管理(三个子路由)|商品管理(四个子路由)

   用户未登录:可以访问login,其余六个路由不能访问(指向login)
   用户登录成功:不可以访问login[指向首页],其余的路由可以访问
 *
 */
