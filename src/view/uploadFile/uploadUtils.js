/**
 * 当前文件是用来处理上传下载的方法
 */


// 用来处理chunks的
import SparkMD5 from 'spark-md5'
const CHUNK_SIZE = 1024 * 1024 * 5 // 5M

// 对大文件进行切片
export function sliceFile2chunk(file) {
    const blobSlice =
        File.prototype.slice ||
        File.prototype.mozslice ||
        File.prototype.webkitSlice
    const fileChunks = []
    if (file.size <= CHUNK_SIZE) {
        fileChunks.push({ file })
    } else {
        let chunkStartIndex = 0
        while (chunkStartIndex < file.size) {
            fileChunks.push({
                files: blobSlice.call(
                    file,
                    chunkStartIndex,
                    chunkStartIndex + CHUNK_SIZE,
                ),
            })
            chunkStartIndex += CHUNK_SIZE
        }
    }
    return fileChunks
}

//  获取文件的hash值
export function getFileHash(file) {
    let hashProcess = 0
    let fileHash = null

    return new Promise((resolve) => {
        const fileChunks = sliceFile2chunk(file)
        const spark = new SparkMD5.ArrayBuffer()
        let hadReadChunksNum = 0 // 已读取的文件块数量
        const readFile = (chunkIndex) => {
            const fileReader = new FileReader()
            fileReader.readAsArrayBuffer(fileChunks[chunkIndex]?.files)
            fileReader.onload = (e) => {
                hadReadChunksNum++
                spark.append(e.target.result)
                if (hadReadChunksNum === fileChunks.length) {
                    hashProcess = 100
                    fileHash = spark.end()
                    fileReader.onload = null
                    resolve(fileHash)
                } else {
                    hashProcess = Math.floor((hadReadChunksNum / fileChunks.length) * 100)
                    readFile(hadReadChunksNum)
                }
            }
        }

        readFile(0)
    })
}

/**
 * status 0: 未上传， 1:上传部分， 2： 上传完成
 * 根据不同状态处理文件块，并加工文件块成待上传文件块
 * @param res
 * @param fileChunks
 */
export function createWait2UploadChunks(res, fileChunks) {
    if (!res.data) {
        return
    }
    const wait2UploadChunks = []
    if (res.data.status === 0) {
        fileChunks.map((chunk, index) => {
            const fileChunk = formateChunks(chunk, index)
            wait2UploadChunks.push(fileChunk)
        })
    }
    if (res.data.status === 1) {
        const restFileChunksIndex = []
        const { hadUploadList } = res.data
        // hadUploadList是服务端返回的已上传的文件块标识 类型是Array
        hadUploadList?.length &&
        hadUploadList.map((item) => {
            restFileChunksIndex.push(item.index)
        })
        fileChunks.map((chunk, index) => {
            if (!restFileChunksIndex.includes(index)) {
                const fileChunk = formateChunks(chunk, index)
                wait2UploadChunks.push(fileChunk)
            }
        })
    }
    if (res.data.status === 2) {
        console.log('服务器上已经有相同的文件，执行妙传操作')
    }

    return wait2UploadChunks
}

// 该函数式对文件块进行标准化，这里可以与后端做协商得出的，看后端需要什么样的数据
// 例如： 服务端需要的文件块数据格式为：{file: File, index: number, partSize: number, fileHash: string}
export function formateChunks(chunk, index) {
    const chunkFormData = new FormData()
    chunkFormData.append('file', chunk.file)
    chunkFormData.append('index', index)
    chunkFormData.append('totalSize', totalSize)
    chunkFormData.append('partSize', chunk.file.size)
    chunkFormData.append('fileHash', fileHash)
    return chunkFormData
}




let currentHttpNum = 0 // 指的是当前正在上传的http请求数
const maxHttpNum = 5 // 最大同时上传的http请求数
let hasUploadedChunkNum = 0 // 已上传的文件块数量
let nextChunkIndex = 4 // 下一个上传的文件块的index
let uploadProgress = 0 // 上传的进度

/**
 * 切割待上传文件块，每次上传5个文件块
 * @param wait2UploadChunks
 * @param maxHttpNum
 */
function uploadFileChunks(wait2UploadChunks, maxHttpNum = 5) {
    const chunksToUpload = wait2UploadChunks.slice(0, maxHttpNum)
        chunksToUpload.forEach(function (item) {
            uploadFileChunk(item).then(error => console.error('Error uploading chunk:', error))
        }, this)
}

/**
 * 上传文件块
 * @param chunkFormData
 * @returns {Promise<void>}
 */
async function uploadFileChunk(chunkFormData) {
    try {
        currentHttpNum++
        const res = await uploadChunkFn(chunkFormData) // uploadChunkFn是执行文件上传的Http请求
        currentHttpNum--
        if (res.code === 200) {
            if (hasUploadedChunkNum < wait2UploadChunks.length) {
                hasUploadedChunkNum++
            }
            // 上传下一个文件块
            if (wait2UploadChunks.length > ++nextChunkIndex) {
                uploadFileChunk(wait2UploadChunks[nextChunkIndex])
            }
            uploadProgress = Math.floor((hasUploadedChunkNum / wait2UploadChunks.length) * 100)
            if (currentHttpNum <= 0) {
                mergeChunks() // 在第五步执行合并函数
            }
        }
    } catch (error) {
        console.error('Error uploading chunk:', error)
    }
}
