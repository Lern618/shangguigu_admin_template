//  商品管理的接口
import request from '@/utils/request'
//   引用品牌管理接口地址
import { APIURL } from '@/api/apiUrl'
//  引用品牌管理接口类型
import { tradeResponse, baseTrade } from '@/api/product/trademark/type'

//  获取商品列表接口
export const reqTradeList = (page: number, limit: number) =>
  request.get<any, tradeResponse>(APIURL.TRADE_LIST + `${page}/${limit}`)

// 新增商品接口
export const addTradeMark = (data: baseTrade) => {
  if (data?.id) {
    return request.put<any, any>(APIURL.UPDATE_ADD_TRADEMARK, data)
  } else {
    return request.post<any, any>(APIURL.ADD_TRADEMARK, data)
  }
}

// 修改商品接口
export const updateTradeMark = () => {
  request.put<any, any>(APIURL.UPDATE_ADD_TRADEMARK)
}
// 删除商品接口
export const deleteTradeMark = (id: number) => {
  return request.delete<any, any>(APIURL.DELETE_TRADEMARK + `/${id}`)
}
