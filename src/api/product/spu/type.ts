// 商品的SPU接口类型
// 基本接口返回类型
export interface baseResponse {
  code: number
  message: string
  ok: boolean
}
//SPU的数据类型
export interface SpuData {
  id?: number | string
  category3Id?: string | number
  spuName?: string
  description?: string
  createTime?: string
  updateTime?: string
  tmId?: number | string
  spuSaleAttrList?: null | spuSaleAttrList[]
  spuImageList?: null | onSaleImg[]
  spuPosterList?: null
}
// 数组： 元素是已有的SPU数据类型
export type Records = SpuData[]

// 获取已有的SPU接口返回的数据ts类型
export interface HadSpuResponseData extends baseResponse {
  data: {
    records: Records[]
    total: number | string
    size: number | string
    current: number | string
    pages: number | string
    searchCount?: boolean
    orders?: []
    optimizeCountSql?: boolean
    hitCount?: boolean
    countId?: null
    maxLimit?: null
  }
}

//  品牌数据的ts类型
export interface TradeMark {
  id: string | number
  createTime?: string
  updateTime?: string
  tmName: string
  logoUrl: string
}

// 品牌数据的返回类型
export interface tradeMarkResponse extends baseResponse {
  data: TradeMark[]
}

// 商品品牌的在售图片
export interface onSaleImg {
  id?: string | number
  spuId?: string | number
  imgName: string | undefined
  imgUrl: string | undefined
  createTime?: string
  updateTime?: string
}
//商品品牌的在售图片接口返回类型
export interface onSaleImgResponse extends baseResponse {
  data: onSaleImg[]
}

// 某个SPU下的在售商品的属性下的某条数据
export interface spuSaleAttrRow {
  id?: number | string
  spuId?: number | string
  baseSaleAttrId: number | string
  saleAttrValueName: string
  saleAttrName?: string
  createTime?: string | null
  updateTime?: string | null
  isChecked?: string | null
}
// 某个SPU下的在售商品的属性下的列表数据
export type spuSaleAttrList = spuSaleAttrRow[]

// 某个SPU下的在售商品的单条属性
export interface spuSaleAttr {
  id?: number | string
  spuId?: number | string
  baseSaleAttrId?: number | string
  saleAttrName: string
  createTime?: string | null
  updateTime?: string | null
  spuSaleAttrValueList: spuSaleAttrList
  flag?: boolean
  newSaleAttr?: string
}

//某个SPU下的在售商品的所有属性
export interface spuSaleAttrResponse extends baseResponse {
  data: spuSaleAttr[]
}

//获取整个项目全部的销售属性接口的类型
export interface baseSaleAttr {
  id?: number | string
  name: string
}

//获取整个项目全部的销售属性接口的返回类型
export interface baseSaleAttrResponse extends baseResponse {
  data: baseSaleAttr[]
}

// 平台属性
export interface skuAttr {
  attrId: string | number //平台属性的Id
  valueId: string | number // 属性值的Id
}

// 平台属性
export interface skuSaleAttr {
  saleAttrId: string | number //属性的Id
  saleAttrValueId: string | number // 属性值的Id
}

// 新增Sku的类型
export interface skuInfo {
  category3Id: string | number // 三级分类的Id

  spuId: string | number // 已有的SPU的id
  tmId: string | number // spu的品牌id
  skuName: string //sku的名字
  price: string | number //sku的价格
  weight: string | number //sku的重量
  skuDesc: string // sku的描述
  skuAttrValueList: skuAttr[] // 平台属性
  skuSaleAttrValueList: skuSaleAttr[] // 销售属性
  skuDefaultImg: string // 默认图片设置
}

// 新增一个Sku属性的接口返回
export interface saveSkuInfoResponse extends baseResponse {
  data: skuInfo[]
}
//
export interface SkuInfoResponse extends baseResponse {
  data: SpuData[]
}
