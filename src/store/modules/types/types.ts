//  小仓库的数据类型
import type { RouteRecordRaw } from 'vue-router'
import type { CategoryObj, AttrList } from '@/api/product/attr/attrType.ts'
export interface userState {
  token: string | null
  menuRoutes: RouteRecordRaw[]
  avatar: string | undefined
  username: string | undefined
  darkStyle?: boolean
  btnPermissionList: string[]
}

export interface useCategoryStoreType {
  Category1List: CategoryObj[]
  categoryId1: string | number
  Category2List: CategoryObj[]
  categoryId2: string | number
  Category3List: CategoryObj[]
  categoryId3: string | number
  attrList: AttrList[]
}
