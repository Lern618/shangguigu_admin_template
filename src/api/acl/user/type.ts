// 商品的SPU接口类型
// 基本接口返回类型
export interface baseResponse {
  code: number
  message: string
  ok: boolean
}
// 用户信息
export interface aclUserInfo {
  id?: string | number
  createTime?: string
  updateTime?: string
  username?: string
  password?: string | number
  name?: string
  phone?: string | number
  roleName?: string
}
//  用户信息列表
export type aclUserInfoList = aclUserInfo[]
//  获取用户列表接口返回参数
export interface aclUserInfoResponse extends baseResponse {
  data: {
    records: aclUserInfoList
    total: number | string
    size: number | string
    current: number | string
    orders?: undefined | null | []
    optimizeCountSql?: boolean
    hitCount?: boolean
    countId?: null
    maxLimit?: null
    searchCount?: boolean
    pages: number | string
  }
}
// 角色信息
export interface roleType {
  id: string | number
  createTime?: string
  updateTime?: string
  roleName?: string
  remark: null
}
// 获取角色信息列表
export interface allRoleTypeResponse extends baseResponse {
  data: {
    assignRoles: roleType[]
    allRolesList: roleType[]
  }
}
//  设置角色
export interface setRoleData {
  roleIdList: number[]
  userId: string | number
}
