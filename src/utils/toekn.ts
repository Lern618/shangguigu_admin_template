// 存储token
export function SET_TOKEN(token: string) {
  localStorage.setItem('TOKEN', token)
}

export function GET_TOKEN() {
  return localStorage.getItem('TOKEN')
}

export function REMOVE_TOKEN() {
  return localStorage.removeItem('TOKEN')
}

// 存储token
export function SET_REFRESH_TOKEN(token: string) {
  localStorage.setItem('REFRESH_TOKEN', token)
}

export function GET_REFRESH_TOKEN() {
  return localStorage.getItem('REFRESH_TOKEN')
}

export function REMOVE_REFRESH_TOKEN() {
  return localStorage.removeItem('REFRESH_TOKEN')
}
