import request from '@/utils/request'
import { APIURL } from '../../apiUrl'
import type { CategoryResponseData, attrDetailResponseData } from './attrType'
import {AttrList} from "./attrType";
//  商品属性列表

// 获取商品属性一级列表
export function getCategory1List() {
  return request.get<any, CategoryResponseData>(APIURL.Category1)
}

// 获取商品属性二级列表
export function getCategory2List(category1Id: number | string) {
  return request.get<any, CategoryResponseData>(
    APIURL.Category2 + `${category1Id}`,
  )
}

// 获取商品属性三级列表
export function getCategory3List(category2Id: number | string) {
  return request.get<any, CategoryResponseData>(
    APIURL.Category3 + `${category2Id}`,
  )
}

// 获取商品属性详细列表
export const attrByCategory3 = (
  category1Id: number | string,
  category2Id: number | string,
  category3Id: number | string,
) => {
  return request.get<any, attrDetailResponseData>(
    APIURL.CategoryDetail + `${category1Id}/${category2Id}/${category3Id}`,
  )
}

// 修改商品属性详情接口
export const  updateCategoryDetail = (data: AttrList) => {
  return request.post<any, any>(
      APIURL.UpdateCategoryDetail, data
  )
}

// 删除商品属性
export const deleteAttrByAttrId = (attrId: number | string) => {
  return request.delete(APIURL.DeleteAttr + attrId)
}
