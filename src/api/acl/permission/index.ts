import request from '@/utils/request.ts'
import { APIURL } from '@/api/apiUrl.ts'
import { menuParam, reqMenuListResponse } from '@/api/acl/permission/type.ts'

// 获取所有的菜单数据
export const queryMenuList = () => {
  return request.get<any, reqMenuListResponse>(APIURL.Query_Menu_List)
}

// 新增|修改 菜单
export const reqAddAndEdit = (data: menuParam) => {
  if (!data.id) {
    return request.post<any, any>(APIURL.Add_New_Menu, data)
  } else {
    return request.put<any, any>(APIURL.Update_Menu, data)
  }
}

// 删除某一菜单
export const reqDelMenu = (id: string | number) => {
  return request.delete<any, any>(APIURL.Delete_Menu + id)
}
