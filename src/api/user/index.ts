// 统一管理项目用户的相关接口
import request from '@/utils/request'
import type { loginFormData, loginResponseData, userInfoResponseData } from './userTypes'

import { APIURL } from '../apiUrl'
// 暴露请求函数
// 登录接口方法
export const reqLogin = (data: loginFormData) =>
  // any, loginResponseData 是因为不知道返回的类型是什么，所以需要用到any
  request.post<any, loginResponseData>(APIURL.LOGIN, data)
// 获取用户信息
export const reqUserInfo = () => request.get<any, userInfoResponseData>(APIURL.USERINFO)
//  退出登录
export const reqLagout = () => request.post<any, any>(APIURL.LOGOUT)
