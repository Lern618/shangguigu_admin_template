import request from "@/utils/request.ts";
import { APIURL } from '@/api/apiUrl'
import type {skuDataListResponse} from './type.ts'
import {skuDataInfoResponse} from "./type.ts";

// 获取所有的sku列表
export const getSkuListData  =  (page:string | number, limit:string | number) =>{
    return request.get<any,skuDataListResponse>(APIURL.getSkuListData + `/${page}/${limit}`)
}

// 删除某一个SKU
export const deleteSku = (skuId:number | string) => {
  return request.delete<any, any>(APIURL.deleteSkuBySkuId + skuId)
}

// 商品的下架
export const  cancelSku = (skuId: number | string) => {
    return request.get<any, any>(APIURL.cancelSku + skuId)
}
// 商品的上架
export const  onSaleSku = (skuId: number | string) => {
    return request.get<any, any>(APIURL.onSaleSku + skuId)
}
//  获取商品的详情信息
export const querySkuDetailBySkuId = (skuId: number | string) => {
    return request.get<any, skuDataInfoResponse>(APIURL.querySkuDetailBySkuId + skuId)
}