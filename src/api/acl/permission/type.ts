//  菜单管理的接口类型


// 基本接口返回类型
export interface baseResponse {
    code: number
    message: string
    ok: boolean
}

// 菜单的基础类型
export interface baseMenu {
    "id"?: string | number,
    "createTime"?: string,
    "updateTime"?: string,
    "pid": string | number
    "name": string
    "code": string
    "toCode": string
    "type": number,
    "status": null,
    "level"?: number,
    "children"?: baseMenu[]
    "select": boolean
}


export interface  reqMenuListResponse extends baseResponse{
    data:baseMenu[]
}


//  新增|修改 menu的基础入参
export interface menuParam {
    id?: number
    code: string | number,
    level: string | number,
    name: string | number,
    pid: string | number,
}