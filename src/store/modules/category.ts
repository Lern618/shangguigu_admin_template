// 创建商品属性的小仓库
import { defineStore } from 'pinia'
import {
  getCategory1List,
  getCategory2List,
  getCategory3List,
  attrByCategory3,
} from '@/api/product/attr/index.ts'
import type { useCategoryStoreType } from 'src/store/modules/types/types.ts'
import type {
  CategoryResponseData,
  attrDetailResponseData,
} from '@/api/product/attr/attrType.ts'
const useCategoryStore = defineStore('CategoryStore', {
  state: (): useCategoryStoreType => {
    return {
      Category1List: [],
      categoryId1: '',
      Category2List: [],
      categoryId2: '',
      Category3List: [],
      categoryId3: '',
      attrList: [],
    }
  },
  actions: {
    //  获取一级列表数据
    async APIgetCategory1List() {
      let result: CategoryResponseData = await getCategory1List()
      if (result.code !== 200) {
        return
      }
      this.Category1List = result?.data || []
    },
    //  获取二级列表数据
    async APIgetCategory2List() {
      let result: CategoryResponseData = await getCategory2List(
        this.categoryId1,
      )
      if (result.code !== 200) {
        return
      }
      this.Category2List = result?.data || []
    },
    //  获取三级列表数据
    async APIgetCategory3List() {
      let result: CategoryResponseData = await getCategory3List(
        this.categoryId2,
      )
      if (result.code !== 200) {
        return
      }
      this.Category3List = result?.data || []
    },
    //  获取商品详细属性
    async APIgetAttrDetail() {
      let result: attrDetailResponseData = await attrByCategory3(
        this.categoryId1,
        this.categoryId2,
        this.categoryId3,
      )
      if (result.code !== 200) {
        return
      }
      this.attrList = result.data
    },
  },
})

export default useCategoryStore
