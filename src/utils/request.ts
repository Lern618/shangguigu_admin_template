import axios from 'axios'
import { ElMessage } from 'element-plus'
import useUserStore from '@/store/modules/user'
// let  userStore = useUserStore()
import { ElLoading } from 'element-plus'
//  封装 AbortController 取消 Axios 请求
import { AxiosCanceler } from './cancelAxios'
const axiosCancelerInstance = new AxiosCanceler()
//创建axios实例
const request = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_API,
  timeout: 5000,
})
//  遮掩层
let loadingInstance = null
//请求拦截器
request.interceptors.request.use((config) => {
  //  将接口请求放入map中，并判断是否是重复的，如果是就丢弃上一次的
  axiosCancelerInstance.addPending(config)
  // 打开遮掩层
  loadingInstance = ElLoading.service({
    lock: true,
    text: '正在努力加载中...',
    background: 'rgba(0, 0, 0, 0.7)',
  })
  //  获取用户的token
  const userStore = useUserStore()
  if (userStore.token) {
    config.headers.token = userStore.token
  }
  return config
})
//响应拦截器
request.interceptors.response.use(
  (response) => {
    // 移除axiosCanceler
    axiosCancelerInstance.removePending(response.config)
    // 关闭遮掩层
    loadingInstance.close()
    return response.data
  },
  (error) => {
    console.log("error", error)
    //处理网络错误
    let msg = ''
    const status = error.response.status
    switch (status) {
      case 401:
        msg = 'token过期'
        break
      case 403:
        msg = '无权访问'
        break
      case 404:
        msg = '请求地址错误'
        break
      case 500:
        msg = '服务器出现问题'
        break
      default:
        msg = '无网络'
    }
    // 移除axiosCanceler
    axiosCancelerInstance.removePending(error.config)
    // 关闭遮掩层
    loadingInstance.close()
    ElMessage({
      type: 'error',
      message: msg,
    })
    return Promise.reject(error)
  },
)
export default request
