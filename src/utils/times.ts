//  获取时间
export const getTime = () => {
  const hours = new Date().getHours()
  console.log(hours)
  if (hours >= 5 && hours <= 9) {
    return '上午'
  } else if (hours >= 10 && hours < 15) {
    return '中午'
  } else if (hours >= 15 && hours <= 19) {
    return '下午'
  } else if (hours > 19 && hours <= 24) {
    return '晚上'
  } else {
    return '凌晨'
  }
}
