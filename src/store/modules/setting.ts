//  创建小仓库，用来管理顶部面包屑图标
import { defineStore } from 'pinia'

const useLayOutSettingStore = defineStore('SettingStore', {
  state: () => {
    return {
      fold: false, // 控制菜单的折叠与收起
      refresh: false, // 控制是否刷新
    }
  },
})

export default useLayOutSettingStore
