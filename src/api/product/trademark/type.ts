// 商品接口的类型
// 基本接口返回类型
export interface baseResponse {
  code: number
  message: string
  ok: boolean
}

//  商品列表接口类型 -records每一条的数据类型
export interface baseTrade {
  id?: number
  // createTime: string
  // updateTime: string
  tmName: string
  logoUrl: string
}

//  商品列表接口类型 -records
export type tradeList = baseTrade[]
// export  interface  IReward<T> {
//     records: T[]
// }

// 商品列表接口类型
export interface tradeResponse extends baseResponse {
  data: {
    records: tradeList
    total: number
    size: number
    current: number
    orders: any
    optimizeCountSql: boolean
    hitCount: boolean
    countId: any
    maxLimit: any
    searchCount: boolean
    pages: number
  }
}
