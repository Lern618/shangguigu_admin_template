import { defineConfig, UserConfigExport, ConfigEnv, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
// path是node提供的一个模块 可以获取到某一个文件或者文件夹的路径
import path from 'path'
import DefineOptions from 'unplugin-vue-define-options/vite'
// 引入svg所需要的图标
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'

import { viteMockServe } from 'vite-plugin-mock'

/**
 *  command  是用来检测当前的开发环境
 */
export default defineConfig(({ command, mode }) => {
  // export default defineConfig(({ command }) => {
  //  获取各个环境下的对应的变量  其中， mode是当前的环境变量， process.cwd() 是指根目录的位置
  const env = loadEnv(mode, process.cwd())
  return {
    plugins: [
      vue(),
      DefineOptions(),
      createSvgIconsPlugin({
        // Specify the icon folder to be cached
        iconDirs: [path.resolve(process.cwd(), 'src/assets/icons')],
        // Specify symbolId format
        symbolId: 'icon-[dir]-[name]',
      }),
      viteMockServe({
        // default
        // enable: true,
        mockPath: 'mock',
        localEnabled: command === 'serve', // 保证开发阶段可以使用mock接口
      }),
    ],
    resolve: {
      alias: {
        '@': path.resolve('./src'), // 相对路径的别名 用@代替 src
      },
    },
    css: {
      preprocessorOptions: {
        scss: {
          javascriptEnabled: true,
          additionalData: '@import "./src/style/variable.scss";',
        },
      },
    },
    //  代理跨域
    server: {
      proxy: {
        [env.VITE_APP_BASE_API]: {
          //  获取数据的服务器地址设置
          target: env.VITE_SERVICE,
          // 需要代理跨域
          changeOrigin: true,
          // 路径重写
          rewrite: (path) => path.replace(/^\/api/, ''),
        },
      },
    },
  }
})
