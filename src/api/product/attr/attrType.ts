//  定义接口基本地返回类型
export interface ResponseData {
  code: number
  message: string
  ok: boolean
}

//  商品属性的ts类型
export interface CategoryObj {
  id: number | string
  createTime?: string
  updateTime?: string
  name: string
  category1Id?: string | number
  category2Id?: string | number
}

// 属性接口返回的数据类型
export interface CategoryResponseData extends ResponseData {
  data: CategoryObj[]
}

// 属性详情接口,单个标签
export interface Tagtype {
  attrId?: string | number
  createTime?: null | string | undefined
  id?: string | number
  updateTime?: null | string | undefined
  valueName: string | number
  flag?:  boolean
}
// 属性详情接口,标签数组
export type tagList = Tagtype[]

//  属性详情接口,data数组
export interface AttrList {
  attrName: string | number
  attrValueList: tagList
  categoryId: number | string
  categoryLevel: number
  createTime?: null | string | undefined
  id?: string | number
  updateTime?: null | string | undefined

}

// 属性详情接口
export interface attrDetailResponseData extends ResponseData {
  data: AttrList[]
}
