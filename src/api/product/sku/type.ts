
export interface baseResponse {
    code: number
    message: string
    ok: boolean
}

//SPU的数据类型
export interface SpuData {
    id?: number | string
    category3Id?: string | number
    spuName?: string
    description?: string
    createTime?: string
    updateTime?: string
    tmId?: number | string
    spuSaleAttrList?: null | spuSaleAttrList[]
    spuImageList?: null | onSaleImg[]
    spuPosterList?: null
}
// 数组： 元素是已有的SPU数据类型
export type Records = SpuData[]
// 某个SPU下的在售商品的属性下的某条数据
export interface spuSaleAttrRow {
    id?: number | string
    spuId?: number | string
    baseSaleAttrId: number | string
    saleAttrValueName: string
    saleAttrName?: string
    createTime?: string | null
    updateTime?: string | null
    isChecked?: string | null
}
// 某个SPU下的在售商品的属性下的列表数据
export type spuSaleAttrList = spuSaleAttrRow[]

// 商品品牌的在售图片
export interface onSaleImg {
    id?: string | number
    spuId?: string | number
    imgName: string | undefined
    imgUrl: string | undefined
    createTime?: string
    updateTime?: string
}

export interface skuDataListResponse extends baseResponse {
    data: {
        records: Records[]
        total: number | string
        size: number | string
        current: number | string
        pages: number | string
        searchCount?: boolean
        orders?: []
        optimizeCountSql?: boolean
        hitCount?: boolean
        countId?: null
        maxLimit?: null
    }
}

export  interface  skuDataInfoResponse extends  baseResponse {
    data: SpuData
}