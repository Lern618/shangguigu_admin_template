//  创建用户相关的小仓库
import { defineStore } from 'pinia'
//  引入接口
import { reqLogin, reqUserInfo, reqLagout } from '@/api/user'
//  引入数据类型
import type {
  loginFormData,
  loginResponseData,
  userInfoResponseData,
} from '@/api/user/userTypes.ts'
import type { userState } from '@/store/modules/types/types'

// 引入深拷贝函数
import deepClone from 'lodash'

// import type { loginResponseData } from '@/api/user/userTypes'
//引入路由(常量路由)
import { constantRoutes, asyncRoutes, anyRoutes } from '@/router/routes.ts'

import { SET_TOKEN, GET_TOKEN, REMOVE_TOKEN } from '@/utils/toekn'
import router from '@/router'
import * as _ from 'lodash'
//  过滤异步路由
const filterAsyncRoute = (asyncRoute, routes) => {
  return asyncRoute.filter((it) => {
    if (routes.includes(it.name)) {
      if (it.children && it.children.length) {
        it.children = filterAsyncRoute(it.children, routes)
      }
      return true
    }
  })
}
//  创建User仓库
const useUserStore = defineStore('User', {
  // 小仓库用来存储数量
  state: (): userState => {
    return {
      token: GET_TOKEN(), // 用户存储的token
      menuRoutes: constantRoutes, //仓库存储生成菜单需要数组(路由)
      avatar: '', // 用户头像
      username: '', // 用户姓名
      darkStyle: false, //开启暗黑模式
      btnPermissionList: [], // 按钮权限列表
    }
  },
  //   逻辑|异步逻辑
  actions: {
    // 用户登录
    async userLogin(data: loginFormData) {
      // 登录请求
      const result: loginResponseData = await reqLogin(data)
      //    登录请求： 200-> token  失败201 -> 登录失败的错误信息
      if (result.code == 200) {
        // pinia 和 vuex 一样是采用的是一个js存储对象
        this.token = <string>result.data
        //  存储token
        SET_TOKEN(result.data as string)

        return true
      } else {
        return Promise.reject(new Error(<string>result.data))
      }
    },
    // 用户信息
    async userInfo() {
      //  获取用户信息
      const result: userInfoResponseData = await reqUserInfo()
      // console.log(result)
      if (result.code !== 200) {
        return Promise.reject(new Error(<string>result.message))
      }
      const data = result.data
      this.username = data.name
      this.avatar = data.avatar
      this.btnPermissionList = data.buttons
      // 过滤异步路由
      let tempAsyncRoute = filterAsyncRoute(
        _.cloneDeep(asyncRoutes),
        data.routes,
      )
      //  路由总的数据
      this.menuRoutes = [...constantRoutes, ...tempAsyncRoute, ...anyRoutes]
      //  追加路由
      ;[...tempAsyncRoute, ...anyRoutes].map((route) => {
        router.addRoute(route)
      })
      // console.log(router.getRoutes())
      return 'ok'
    },
    // 用户退出登录
    async userLoginout() {
      //  用户退出登录
      const result = await reqLagout()

      if (result.code == 200) {
        this.token = ''
        this.username = ''
        this.avatar = ''
        REMOVE_TOKEN()
        return Promise.resolve(true)
      } else {
        return Promise.reject(new Error(result.message))
      }
    },
    // 修改暗黑模式
    userSettingDark(val: boolean) {
      this.darkStyle = val
    },
  },
  getters: {},
})

export default useUserStore
