import request from '@/utils/request'
import { APIURL } from '@/api/apiUrl'
import type { aclUserInfoResponse } from './type'
import { aclUserInfo, allRoleTypeResponse, setRoleData } from './type'
// 获取所有的用户信息
export const queryUserList = (
  page: string | number,
  limit: string | number,
  username: string,
) => {
  return request.get<any, aclUserInfoResponse>(
    APIURL.queryUserList + `${page}/${limit}?username=${username}`,
  )
}
// 新增|修改用户
export const addAndUpdateUserInfo = (userInfo: aclUserInfo) => {
  if (!userInfo.id) {
    return request.post<any, any>(APIURL.addNewUser_ACL, userInfo)
  }
  return request.put<any, any>(APIURL.updateUser_ACL, userInfo)
}

// 获取所有的用户角色列表
export const queryRoleListByAdminId = (adminId: string | number) => {
  return request.get<any, allRoleTypeResponse>(
    APIURL.Get_User_Role + `${adminId}`,
  )
}

// 根据用户分配角色
export const assignedRole = (data: setRoleData) => {
  return request.post<any, any>(APIURL.Assigned_Role, data)
}

// 删除角色信息
export const deleteRole = (id: string | number) => {
  return request.delete(APIURL.Delete_Role + id)
}
// 批量删除角色信息
export const batchDeleteRole = (idList: string[] | number[]) => {
  return request.delete(APIURL.Batch_Delete_Role, { data: idList })
}
