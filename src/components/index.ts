import SvgIcon from './SvgIcon/index.vue'
import Pagination from './Pagination/index.vue'
import Category from '@/components/Category/index.vue'
//  引用所有的icon
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

//  引用自定义的组件
import MyRadio from "@/view/myTest/encapsulatedComponents/myRadio/myRadio.vue";
import MyCheckBox from '@/view/myTest/encapsulatedComponents/myCheckBox/myCheckBox.vue';


//  需要注册的全局组件
const allComponents = {
  SvgIcon,
  Pagination,
  Category,
  MyRadio,
  MyCheckBox,
}

export default {
  //  如果要全局使用，必须使用install方法，该方法在main中会被安装
  install(app: any): void {
    // console.log(app)
    Object.keys(allComponents).forEach((itemComponent: string) => {
      // console.log(itemComponent)
      app.component(itemComponent, allComponents[itemComponent])
    })

    //  将所有的icon注册为全局组件
    for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
      app.component(key, component)
    }
  },
}
