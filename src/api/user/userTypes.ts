// 登录接口需要所携带的参数ts类型
export interface loginFormData {
  username: string
  password: string
}

//  定义接口基本地返回类型
export interface ResponseData {
  code: number
  message: string
  ok: boolean
}

// 定义登录接口返回的数据类型
export interface loginResponseData extends ResponseData {
  data: string
}

// 定义获取用户信息接口的数据类型
export interface userInfo {
  routes: string[]
  buttons: string[]
  roles: string[]
  name: string
  avatar: string
}
export interface userInfoResponseData extends ResponseData {
  data: userInfo
}
