import ElementMap from './element_map'
import * as _ from "lodash";

export function computedFormItem(itemConfig: any, form) {
    const item = _.cloneDeep(itemConfig)

    // 根据配置计算组件的名称
    const res = ElementMap[item.type || 'input']
    item.type = res.component
    if (res.props) {
        item.props = { ...res.props, ...item.props }
    }

    // 处理联动问题
    if(item?.getProps && isFunction(item.getProps)) {
        Object.assign(item.props, item.getProps(form))
    }
    item._isShow = item?.isShow && isFunction(item.isShow) ? !!item.isShow(form) : true

    //  处理校验
    item._rule = item.rules && isFunction(item.rules) ? item.rules(form) : item.rules

    if(!item._isShow) {
        delete form[item.key]
    }

    return item
}



// 判断是否是函数
function isFunction (fn) {
    return Object.prototype.toString.call(fn).slice(8, -1) === 'Function'
}