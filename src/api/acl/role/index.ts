import request from '@/utils/request'
import { APIURL } from '@/api/apiUrl'
import {
  permissionListResponse,
  role,
  roleListResponse,
} from '@/api/acl/role/type'

// 角色管理： 查询所有的角色信息
export const queryRoleList = (
  page: number,
  limit: number,
  roleName: string,
) => {
  return request.get<any, roleListResponse>(
    APIURL.Query_Role_List + `${page}/${limit}?roleName=${roleName}`,
  )
}

//  新增|编辑一个角色
export const reqAddEditRole = (data: role) => {
  if (data.id) {
    return request.put<any, any>(APIURL.Update_Role_Id, data)
  } else {
    return request.post<any, any>(APIURL.Add_New_Role, data)
  }
}
//  根据角色ID获取所有的职位
export const reqGetRoleByRoleId = (roleId: number | string) => {
  return request.get<any, permissionListResponse>(
    APIURL.Quey_RoleList_By_RoleId + roleId,
  )
}
// 给对应的职位分配职位
export const reqSetPermission = (
  roleId: number | string,
  permissionId: number[] | string[],
) => {
  return request.post<any, any>(
    APIURL.Set_Permission + `?roleId=${roleId}&permissionId=${permissionId}`,
  )
}
// 删除角色
export const reqDelRole = (id: number | string) => {
  return request.delete<any, any>(APIURL.Delete_role + id)
}
