import axios from 'axios'
import { GET_REFRESH_TOKEN, GET_TOKEN, SET_TOKEN } from './toekn'
import { useRouter } from 'vue-router'
import { ElMessage } from 'element-plus'

//获取路由器对象
let $router = useRouter()

// 是否处于刷新状态
let refreshTokenFlag = false

// 重试队列，每一项将是一个待执行的函数形式
let retryRequests = []

// 创建一个接口请求实例
const instance = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_API,
  timeout: 50000,
  // withCredentials: true, // cookie跨域必备
  headers: {
    'Content-Type': 'application/json',
    'X-Requested-With': 'XMLHttpRequest', // 不刷新页面的情况下请求特定 URL
  },
})

// 发送前接口请求拦截
instance.interceptors.request.use((config) => {
  // 获取短期token
  const token = GET_TOKEN()
  if (token) {
    config.headers.token = token
  } else {
    $router.push('/home')
  }
})

//
instance.interceptors.response.use(
  (response) => {
    const res = response.data
    //  判断数据是否合理
    if (res.code !== 0) {
      if (res.message) {
        ElMessage({
          type: 'error',
          message: res.message,
        })
      }
      return Promise.reject(res)
    } else {
      return response.data
    }
  },
  (error) => {
    if (!error.response) return Promise.reject(error)
    // 根据refreshtoken重新获取token
    // 5000系统繁忙
    // 5001参数错误
    // 1003该用户权限不足以访问该资源接口
    // 1004访问此资源需要完全的身份验证
    // 1001access_token无效
    // 1002refresh_token无效
    if (
      error.response.data.code === 1004 ||
      error.response.data.code === 1001
    ) {
      const errconfig = error.config
      // 立即刷新token
      if (!refreshTokenFlag) {
        refreshTokenFlag = true
        // 刷新token
        return getRefreshTokenFunc()
          .then((res) => {
            // 刷新完成后 重置token
            SET_TOKEN(res.data.token)
            // 拿到新的token
            errconfig.token = res.data.token || ''
            retryRequests.forEach((cb) => cb(GET_TOKEN()))
            // 重试完清空这个队列
            retryRequests = []
            // 这边不需要baseURL是因为会重新请求url，url中已经包含baseURL的部分了
            errconfig.baseURL = ''
            return instance(errconfig)
          })
          .catch(() => {
            //  刷新失败 refreshing失败 或者是接口请求失败 直接返回到登录页面
            $router.push('/home')
          })
          .finally(() => {
            refreshTokenFlag = false
          })
      } else {
        //  正在刷新token  返回一个未执行resolve的promise，对于后面请求的需要储存起来放到retryRequests中
        return new Promise((resolve) => {
          retryRequests.push((token) => {
            errconfig.baseURL = ''
            errconfig.headers.token = token
            resolve(instance(errconfig))
          })
        })
      }
    } else {
      ElMessage({
        type: 'error',
        message: error.message,
      })
      return Promise.reject(error)
    }
  },
)

// 刷新token的请求方法
function getRefreshTokenFunc() {
  let params = {
    refresh_token: GET_REFRESH_TOKEN() || '',
  }
  return axios.post(api.baseUrl + 'auth-center/auth/refresh_token', params)
}

/**
 * []请求
 * @param params  参数
 * @param operation     接口
 */
function customRequest(url, method, data) {
  // service.defaults.headers['Content-Type']=contentType
  let datatype = method.toLocaleLowerCase() === 'get' ? 'params' : 'data'
  return instance({
    url: url,
    method: method,
    [datatype]: data,
  })
}

export { instance, customRequest }
