//  商品管理的接口
import request from '@/utils/request'
//   引用品牌管理接口地址
import { APIURL } from '@/api/apiUrl'
// 获取接口的类型
import type {
  tradeMarkResponse,
  HadSpuResponseData,
  onSaleImgResponse,
} from './type'
import {
  baseSaleAttrResponse,
  saveSkuInfoResponse,
  SkuInfoResponse,
  SpuData,
  spuSaleAttrResponse,
} from './type'

//  获取商品的spu列表 根据三级列表的id
export const querySpuListByCategory3Id = (
  page: number,
  limit: number,
  category3Id: string | number,
) => {
  return request.get<any, HadSpuResponseData>(
    APIURL.querySpuListByCategory3Id +
      `/${page}/${limit}?category3Id=${category3Id}`,
  )
}

// 获取所有的品牌名称
export const getAllTradeMark = () => {
  return request.get<any, tradeMarkResponse>(APIURL.getAllTradeMark)
}
// 获取某个SPU下的全部售卖的图片数据
export const getAllOnSaleImage = (spuId: number | string) => {
  return request.get<any, onSaleImgResponse>(
    APIURL.getAllOnSaleImage + `/${spuId}`,
  )
}
// 某个Spu下全部的已有的销售属性
export const getAllSaleAttrList = (spuId: number | string) => {
  return request.get<any, spuSaleAttrResponse>(
    APIURL.getAllSaleAttrList + `/${spuId}`,
  )
}
// 获取整个项目全部的销售属性列表【颜色、版本、尺码】
export const getAllBaseAttr = () => {
  return request.get<any, baseSaleAttrResponse>(APIURL.getAllBaseAttr)
}
// 新增/修改一个SPU
export const addAndEditSpu = (data: SpuData) => {
  //  修改
  if (data && data.id) {
    return request.post<any, any>(APIURL.updateSpu, data)
  }
  //  新增
  return request.post<any, any>(APIURL.addNewSpu, data)
}
// 新增一个sku，给某个三级分类下，某个品牌
export const addNewSku = (data:any) => {
  return request.post<any, saveSkuInfoResponse>(APIURL.addNewSku, data)
}
// 通过spuId查询所有的在售商品
export const queryOnSaleSku = (spuId: number | string) => {
  return request.get<any, SkuInfoResponse>(APIURL.queryOnSaleSku + `/${spuId}`)
}
// 通过spuId 删除sku
export const deleteSku = (spuId: number | string) => {
  return request.delete<any, any>(APIURL.deleteSkuUrl + `/${spuId}`)
}
