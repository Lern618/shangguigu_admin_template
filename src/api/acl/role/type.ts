// 角色管理的接口类型
// 基本接口返回类型
export interface baseResponse {
  code: number
  message: string
  ok: boolean
}

// 角色基础信息
export interface role {
  id?: number | string
  createTime?: string
  updateTime?: string
  roleName: string
  remark?: null
}
// 角色基本信息列表
export type roleList = role[]

// 角色管理查询的接口类型
export interface roleListResponse extends baseResponse {
  data: {
    records: roleList
    total: number
    size: number
    current: number
    pages: number
    orders?: [] | null | undefined
    optimizeCountSql: boolean
    hitCount: boolean
    countId: null
    maxLimit: null
    searchCount: boolean
  }
}
// 菜单权限
export interface basepermission {
  id?: number | string
  createTime: string
  updateTime: string
  pid: number | string
  name: string
  code: string
  toCode: string
  type: number | string
  status: null
  level: number | string
  children: []
  select: boolean
}
// 根据角色获取菜单接口
export interface permissionListResponse extends baseResponse {
  data: basepermission[]
}
