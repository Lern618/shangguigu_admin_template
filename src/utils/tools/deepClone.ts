//  通过js自带的structuredClone函数和lodash工具库中的cloneDeep函数生成一个拷贝函数

//  引入lodash
import * as _ from 'lodash'
export function deepClone (data) {
    try{
        if(Object.prototype.toString.call(structuredClone).slice(8, -1) === 'Function') {
            return structuredClone(data)
        }else{
            return _.cloneDeep(data)
        }
    }catch(e) {
        return _.cloneDeep(data)
    }
}
