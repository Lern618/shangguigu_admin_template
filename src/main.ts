import { createApp } from 'vue'
import App from './App.vue'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'

// 引用element-plus 组件库
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// 引入svg 需要的插件
import 'virtual:svg-icons-register'
// 引入全局组件
import allGlobalComponents from '@/components/index.ts'
// 引入全局的scss文件
import '@/style/index.scss'
// 引入路由
import router from './router'
// 引入仓库
import pinia from './store'
// 引入暗黑模式需要的样式
import 'element-plus/theme-chalk/dark/css-vars.css'
//  引入全局路由守卫，路由鉴权
import './permission'
//  引入自定义指令
import { hasBtnPermission } from '@/directive/hasBtnPermission'

const app = createApp(App)
app.use(ElementPlus, {
  locale: zhCn, // 配置element-plus 国际化
})
// // 挂载全局的深拷贝函数
// app.config.globalProperties.$cloneDeep = cloneDeep

// 安装自定义组件
app.use(allGlobalComponents)
// 注册模板路由
app.use(router)
//  安装仓库
app.use(pinia)

// console.log(import.meta.env)
// 自定义指令执行
hasBtnPermission(app)
app.mount('#app')
