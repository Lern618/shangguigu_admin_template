// 测试网络请求的并发
// 并发请求函数





export async function asyncPool(poolLimit, array, iteratorFn) {
    const ret = [] // 用于存放运行的promise数组
    const executing = [] // 存放并发限制的处于Pending状态的Promise对象
    for(const item of array){
        const p = Promise.resolve().then(() => {iteratorFn(item)}) // 兼容iteratorFn是同步函数的场景
        ret.push(p)
        if(poolLimit < array.length) {
            // p执行完成之后，剔除executing数组中的e,保证excuting数组中只有当前正在执行的promise
            const e = p.then(() => {executing.splice(executing.indexOf(e), 1)})
            executing.push(e);
            if(executing.length >= poolLimit) {
                await Promise.race(executing)
            }
        }
    }
    return Promise.all(ret)
}













export const concurrencyRequest = (urls, maxNum) => {
    return new Promise((resolve) => {
        if (urls.length === 0) {
            resolve([]);
            return;
        }
        const results = [];
        let index = 0; // 下一个请求的下标
        let count = 0; // 当前请求完成的数量

        // 发送请求
        async function request() {
            if (index === urls.length) return;
            const i = index; // 保存序号，使result和urls相对应
            const url = urls[index];
            index++;
            console.log(url);
            try {
                const resp = await fetch(url);
                // resp 加入到results
                results[i] = resp;
            } catch (err) {
                // err 加入到results
                results[i] = err;
            } finally {
                count++;
                // 判断是否所有的请求都已完成
                if (count === urls.length) {
                    console.log('完成了');
                    resolve(results);
                }
                request();
            }
        }

        // maxNum和urls.length取最小进行调用
        const times = Math.min(maxNum, urls.length);
        for(let i = 0; i < times; i++) {
            request();
        }
    })
}


