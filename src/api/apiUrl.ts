// 统一管理接口， 枚举   该文件是为了避免接口的地址重复

export enum APIURL {
  //  删除某一个菜单
  Delete_Menu = '/admin/acl/permission/remove/',
  // 添加新的菜单
  Add_New_Menu = '/admin/acl/permission/save',
  // 修改已有菜单
  Update_Menu = '/admin/acl/permission/update',
  // 获取菜单数据
  Query_Menu_List = '/admin/acl/permission',
  // 删除角色
  Delete_role = '/admin/acl/role/remove/',
  // 给相应的职位分配权限
  Set_Permission = '/admin/acl/permission/doAssign',
  // 根据角色ID获取所有的职位
  Quey_RoleList_By_RoleId = '/admin/acl/permission/toAssign/',
  //  新增一个职位
  Add_New_Role = '/admin/acl/role/save',
  //  修改 一个已有的职位
  Update_Role_Id = '/admin/acl/role/update',
  //  查询所有的职位列表
  Query_Role_List = '/admin/acl/role/',
  //  =============================职位管理=======================
  // 批量删除角色
  Batch_Delete_Role = '/admin/acl/user/batchRemove',
  // 删除某一个角色
  Delete_Role = '/admin/acl/user/remove/',
  // 根据角色 分配角色
  Assigned_Role = '/admin/acl/user/doAssignRole',
  // 根据角色ID 获取所有的可选角色 和当前已扮演的角色
  Get_User_Role = '/admin/acl/user/toAssign/',
  // 新增管理用户
  addNewUser_ACL = '/admin/acl/user/save',
  // 修改管理用户
  updateUser_ACL = '/admin/acl/user/update',
  // 获取所有的用户角色
  queryUserList = '/admin/acl/user/',
  // =========================用户管理↑============================
  // 获取SKU详情的接口
  querySkuDetailBySkuId = '/admin/product/getSkuInfo/',
  // sku的下架
  cancelSku = '/admin/product/cancelSale/',
  // sku的上架
  onSaleSku = '/admin/product/onSale/',
  // 删除SKU
  deleteSkuBySkuId = '/admin/product/deleteSku/',
  // 获取sku的列表数据
  getSkuListData = '/admin/product/list',
  // =============================分割线========================
  //  获取商品的spu列表 根据三级列表的id
  querySpuListByCategory3Id = '/admin/product',
  //  获取所有的品牌名称
  getAllTradeMark = '/admin/product/baseTrademark/getTrademarkList',
  // 获取某个SPU的在售的所有商品图片
  getAllOnSaleImage = '/admin/product/spuImageList',
  // 获取某个SPU下的在售商品的属性列表
  getAllSaleAttrList = '/admin/product/spuSaleAttrList',
  // 获取整个项目全部的销售属性列表【颜色、版本、尺码】
  getAllBaseAttr = '/admin/product/baseSaleAttrList',
  // 增加一个新的SPU
  addNewSpu = '/admin/product/saveSpuInfo',
  // 修改一个已有的SPU
  updateSpu = '/admin/product/updateSpuInfo',
  // 添加一个新的SKU
  addNewSku = '/admin/product/saveSkuInfo',
  // 查看某一个SPU下面的所有售卖商品
  queryOnSaleSku = `/admin/product/findBySpuId`,
  // 删除SKU接口
  deleteSkuUrl = '/admin/product/deleteSpu',
  // ===============================分割线====================================
  // 商品属性一级列表
  Category1 = '/admin/product/getCategory1',
  // 商品属性二级列表
  Category2 = '/admin/product/getCategory2/',
  // 商品属性三级列表
  Category3 = '/admin/product/getCategory3/',
  // 商品属性详细信息接口
  CategoryDetail = '/admin/product/attrInfoList/',
  // 新增和修改商品属性详情
  UpdateCategoryDetail = '/admin/product/saveAttrInfo',
  // 删除商品属性
  DeleteAttr = '/admin/product/deleteAttr/',
  // ===============================分割线====================================
  // 获取商品列表接口
  TRADE_LIST = '/admin/product/baseTrademark/',
  //  新增商品接口
  ADD_TRADEMARK = '/admin/product/baseTrademark/save',
  //  修改商品接口
  UPDATE_ADD_TRADEMARK = '/admin/product/baseTrademark/update',
  // 删除已有商品
  DELETE_TRADEMARK = '/admin/product/baseTrademark/remove/',
  // ===================分割线===========================
  // 用户登录路径
  LOGIN = '/admin/acl/index/login',
  // 获取用户信息接口
  USERINFO = '/admin/acl/index/info',
  // 用户注销登录接口
  LOGOUT = '/admin/acl/index/logout',
}
